import UIKit

class WeatherViewController: BaseController {
    private let backgroundView = UIImageView(image: Res.Images.background).then {
        $0.contentMode = .scaleAspectFill
    }
    
    private let weatheryView = WeatheryView()
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
}

extension WeatherViewController {
    override func setupViews() {
        super.setupViews()
        view.addView(backgroundView)
        view.addView(weatheryView)
    }
    
    override func layoutViews() {
        super.layoutViews()
        let margin = view.layoutMarginsGuide

        NSLayoutConstraint.activate([
            backgroundView.topAnchor.constraint(equalTo: view.topAnchor),
            backgroundView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            backgroundView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            backgroundView.bottomAnchor.constraint(equalTo: view.bottomAnchor),
            
            weatheryView.topAnchor.constraint(equalTo: margin.topAnchor),
            weatheryView.leadingAnchor.constraint(equalTo: margin.leadingAnchor),
            weatheryView.trailingAnchor.constraint(equalTo: margin.trailingAnchor),
            weatheryView.bottomAnchor.constraint(equalTo: margin.bottomAnchor)
            
        ])
    }
    
    override func configureAppearance() {
        super.configureAppearance()
        weatheryView.configure()
    }
}
