import UIKit

final class WeatheryView: BaseView {

    let locationButton = UIButton(type: .system).then {
        $0.setImage(Res.Images.location, for: .normal)
    }
    
    private let searchTextField = UITextField().then {
        $0.font = .preferredFont(forTextStyle: .title1)
        $0.setContentHuggingPriority(UILayoutPriority(rawValue: 249), for: .horizontal)
        $0.placeholder = Res.Strings.search
        $0.textAlignment = .right
        $0.borderStyle = .roundedRect
        $0.backgroundColor = .systemFill
    }
    
    private let searchButton = UIButton(type: .system).then {
        $0.setImage(Res.Images.search, for: .normal)
    }
    
    private let conditionImageView = UIImageView(image: UIImage(systemName: "bell", withConfiguration: UIImage.SymbolConfiguration(pointSize: Res.Sizes.buttonSizeSmall)))
    private lazy var temperatureLabel = UILabel().then {
        $0.font = .systemFont(ofSize: 80)
        $0.attributedText = makeTemperatureText(with: "21")
    }
    private let cityLabel = UILabel().then {
        $0.font = .preferredFont(forTextStyle: .largeTitle)
        $0.text = "London"
    }
    
    private lazy var searchStackView = UIStackView(arrangedSubviews: [
        locationButton, searchTextField, searchButton]).then {
        $0.spacing = 8
    }
    
    private lazy var rootStackView = UIStackView(arrangedSubviews: [
        searchStackView, conditionImageView, temperatureLabel, cityLabel]).then {
        $0.axis = .vertical
        $0.alignment = .trailing
        $0.spacing = 10
    }
    
    private func makeTemperatureText(with temperature: String) -> NSAttributedString {
        var boldTextAttributes = [NSAttributedString.Key: AnyObject]()
        boldTextAttributes[.foregroundColor] = UIColor.label
        boldTextAttributes[.font] = UIFont.boldSystemFont(ofSize: 100)
        
        var plainTextAttributes = [NSAttributedString.Key: AnyObject]()
        plainTextAttributes[.font] = UIFont.systemFont(ofSize: 80)
        
        let text = NSMutableAttributedString(string: temperature, attributes: boldTextAttributes)
        text.append(NSAttributedString(string: "°C", attributes: plainTextAttributes))
        
        return text
    }
    
    func configure() {
        
    }
}

extension WeatheryView {
    override func setupViews() {
        super.setupViews()
        addView(rootStackView)
    }
    
    override func layoutViews() {
        super.layoutViews()
        NSLayoutConstraint.activate([
            rootStackView.topAnchor.constraint(equalTo: topAnchor),
            rootStackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            rootStackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            
            searchStackView.widthAnchor.constraint(equalTo: rootStackView.widthAnchor)
        ])
    }
    
    override func configureAppearance() {
        super.configureAppearance()
    }
}
