import UIKit

enum Res {
    enum Strings {
        static let search = "Search"
    }
    enum ImageNames {
        static let location = "location.circle.fill"
        static let search = "magnifyingglass"
    }
    
    enum Images {
        static let location = UIImage(systemName: Res.ImageNames.location, withConfiguration: UIImage.SymbolConfiguration(pointSize: Res.Sizes.buttonSizeSmall))
        static let search = UIImage(systemName: Res.ImageNames.search, withConfiguration: UIImage.SymbolConfiguration(pointSize: Res.Sizes.buttonSizeSmall))
        static let background = #imageLiteral(resourceName: "background")
    }
    
    enum Sizes {
        static let buttonSizeSmall = 30.0
    }
}
